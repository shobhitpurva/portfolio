import React from 'react'
import "./FlipCard.css";
const FlipCardSkills = ({
        headImage,
        heading,
        subhead,
        back1,
        back2,
        back3,
        back4,
        back5,
        back6,
        back7,
        back8,
        back9,
        back10,
        img1,
        img2,
        img3,
        img4,
        img5,
        img6,
        img7,
        img8,
        img9,
        img10,

    }) => {
    return (
        <div className="flipCard">
            <div className="skills">
                <div className="face front">
                    <img src={headImage}></img>
                    <div className="text">
                        <h2>{heading}</h2>
                        <h3>
                            {subhead}
                        </h3>
                    </div>
                </div>
                <div className="face back">
                    <div className="back__info">
                    <div className="detail">
                        <img className="detail__img" src={img1}></img>
                        <h2 className="detail__text">{back1}</h2>
                    </div>
                    <div className="detail">
                        <img className="detail__img" src={img2}></img>
                        <h2 className="detail__text">{back2}</h2>
                    </div>
                    <div className="detail">
                        <img className="detail__img" src={img3}></img>
                        <h2 className="detail__text">{back3}</h2>
                    </div>
                    <div className="detail">
                        <img className="detail__img" src={img4}></img>
                        <h2 className="detail__text">{back4}</h2>
                    </div>
                    <div className="detail">
                        <img className="detail__img" src={img5}></img>
                        <h2 className="detail__text">{back5}</h2>
                    </div>
                    <div className="detail">
                        <img className="detail__img" src={img6}></img>
                        <h2 className="detail__text">{back6}</h2>
                    </div>
                    <div className="detail">
                        <img className="detail__img" src={img7}></img>
                        <h2 className="detail__text">{back7}</h2>
                    </div>
                   </div>
                </div>
            </div>
        </div>
    )
}
export default FlipCardSkills;

//figma, adobe xd, vs code, git hub/bitbucket,
//photoshop, filmora, notion
//