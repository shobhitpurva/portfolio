import React from 'react'
import "./Page5.css"
import Slider from "react-slick";
import Testimonial from "./Testimonial"
import "slick-carousel/slick/slick.css";
import "slick-carousel/slick/slick-theme.css";
import Zoom from 'react-reveal/Zoom';

const Page5 = () => {
    let settings = {
        arrows: false,
        autoplay: true,
        autoplaySpeed: 2000,
        dots: true,
        infinite: true,
        speed:500,
        slidesToShow: 1,
        slidesToScroll: 1,
        cssEase: "linear",  
    } 
    return (
        <Zoom>
        <div className="page5">
            <Slider {...settings}>
                
            <Testimonial 
                     testImg="https://i.imgur.com/sBiB4xY.png" 
                     message="It's been great working with Shobhit Purva,
                        I've never been around a tech expert not just
                        a tech expert he's great with his skills and
                        designing websites.He actually understands
                        what a high performance mindset is. He not only
                        work hard but he also works smart, Shobhit is so 
                        dedicated and driven to get better in every aspect
                        of life."
                     nameTitle="One Coffee"
                     workTitle="Coffee Shop"
                />
                <Testimonial 
                     testImg="https://i.imgur.com/uafXm89.png" 
                     message="Shobhit Purva had demonstrated great
                     technical skills with a self-motivated attitude
                     towards learning new things. During his internship
                     his team work helped out in building an amazing website from scratch
                     along with that he had proposed many marketing strategies.
                     Overall his performance exceeded our expectations,
                     we wish him all the best for his future endeavours.
                     "
                     nameTitle="AS Technosoft Private Limited"
                     workTitle="BI Analytics & Consulting Services"
                />
                <Testimonial
                    testImg="https://i.imgur.com/fhBGxTK.png"
                    message="Thankyou Shobhit for your all your help. 
                    You are truly dedicated and ambitous towards your work,
                    you basically made our work lot easier by making such a perfect logo and design theme that suits our restaurant.
                    I truly mean it, it gave us new ideas and positive vibe, all of our  customers loved it. 
                    Your desinging skills are really good. Your hardwork and kindness are truely reflected in your work."
                    nameTitle="Hunger Strike"
                    workTitle="Restaurant"
                />
                <Testimonial
                    testImg="https://i.imgur.com/7mM3kMs.jpg" 
                    message="I knew few concepts of javascript when I started to take Shobhit's session.
                    But little did I know I have only covered 10% of it and there was much more to know.
                    It was a perfect blend of theory and practical explanation got to know lot of things like how our programs are compiled in the background ,what steps are involved.
                    This helped me to debug errors quite easily.
                    More practical and real world examples while teaching a concept it made me understand things even better.
                    Overall it was an amazing experience"
                    nameTitle="Veena"
                    workTitle="Web Developer"
                />
                <Testimonial 
                     testImg="https://i.imgur.com/NOZWE7R.jpg" 
                     message="We worked on web development together.
                        Being with a person of same age group creates
                        a better environment and having a great mentor
                        as well as teacher like him is a great honour.
                        We worked flawlessly without any communication gap,
                        his humble nature, ideas and curiosity made things
                        go very smooth. Overall experience was wonderful,
                        I have gained so much knowledge not only in web
                        development but also other fields.
                        He guided me to explore different fields
                        and find my interest."
                     nameTitle="Diyvam"
                     workTitle="Student at MNIT Jaipur"
                />
                <Testimonial 
                     testImg="https://i.imgur.com/PvQnYJ3.jpg" 
                     message="Because of Shobhit I got into coding field till 
                    then I did not know what the actual stuff was. I thought coding 
                    was really a difficult part but after getting through Shobhit's 
                    teaching and watching all of the videos I got to know how easy was coding and
                    after that I starting getting into it and my interest towards coding started 
                    increasing gradually.
                    Thanks a lot Shobhit for everything, I will always and forever be grateful"
                     nameTitle="Varsha"
                     workTitle="Programmer"
                />
                 <Testimonial 
                    testImg="https://i.imgur.com/LHkNVgO.jpg"
                    message="Shobhit's innovative teaching methods
                    has been a big inspiration for me. His enthusiasm
                    for web development motivates me to learn more."
                    nameTitle="Sahana"
                    workTitle="Programmer"
                />
                
                {/* <Testimonial 
                     testImg="https://i.imgur.com/uafXm89.png" 
                     message="I have mentored Shobhit Purva at ASMI.
                         As an intern he showed great a dedicatation towards his work
                         He huddled all tasks and problems that came along the way.
                         Having a passion towards Computer Science and programming
                         with a buring desire for learning new things - proved nothing is impossible.
                         Always try a little harder to be a little better. And here's wishing you the very
                        best for all your new ventures.
                     "
                     nameTitle="AS Technosoft Private Limited"
                     workTitle="BI Analytics & Consulting Services"
                /> */}
            </Slider>
        </div>
        </Zoom>
    )
}

export default Page5;


