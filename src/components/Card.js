import React from 'react'
import "./Card.css";
import Button from '@material-ui/core/Button';
import ArrowRightAltIcon from '@material-ui/icons/ArrowRightAlt';
import Fade from 'react-reveal/Fade';
// import styled from 'styled-components';
// import { useSpring, animated, config } from 'react-spring';

// const Container = styled(animated.div)``;

// const calc = (x, y) => [-(y - window.innerHeight / 2) / 20, (x - window.innerWidth / 2) / 20, 1]
// const trans = (x, y, s) => `perspective(2000px) rotateX(${x}deg) rotateY(${y}deg) scale(${s})`

const Card = ({ projectName,
                projectImage,
                Detail1,
                languageLogo1,
                languageName1,
                languageLogo2,
                languageName2,
                languageLogo3, 
                languageName3,
                languageLogo4, 
                languageName4,
                container,
                projectCode,
                projectLink,
            }) => {
//     const [props, set] = useSpring(() => ({ xys: [0, 0, 1] , config: config.default}));

    
    return (
        <div className="card">
            <div className={container}>
                <Fade right>
                    <img className="card__projectImage" src={projectImage}/>
                </Fade>
                <Fade left>
                    <div className="card__details">
                        <h3>{projectName}</h3>
                        <p> 
                            <h3 className="card__info">{Detail1}</h3>
                            <h3 className="card__skillsHead">Build Using</h3>
                            <div className="card__skills">
                                <div className="card__languages">
                                    <img className="card__logo" src={languageLogo1} />
                                    <p className="card__languageName">{languageName1}</p>
                                </div>
                                <div className="card__languages">
                                    <img className="card__logo" src={languageLogo2} />
                                    <p className="card__languageName">{languageName2}</p>
                                </div>
                                <div className="card__languages">
                                    <img className="card__logo" src={languageLogo3}/>
                                    <p className="card__languageName">{languageName3}</p>
                                </div>
                                {/* <div className="card__languages">
                                    <img className="card__logo" src={languageLogo4} />
                                    <p className="card__languageName">{languageName4}</p>
                                </div> */}
                            </div>
                            <div className="card__bottomButtons">
                            <a href={projectCode}><button className="card__bottomButton1">
                                <img className="card__bottomImage" src="https://i.imgur.com/IlXnxSc.png" />
                                Source Code
                            </button></a>
                            <a href={projectLink}><button className="card__bottomButton2">
                                View Project
                                <ArrowRightAltIcon />
                            </button></a>
                            </div>
                        </p>
                    </div>
                </Fade>
            </div>
        </div>

        // <Container className="card" 
        //     onMouseMove={({clientX: x, clientY: y}) => (set({xys: calc(x, y)}))}
        //     onMouseLeave={() => set({xys:[0,0,1]})}
        //     style={{
        //         transform: props.xys.interpolate(trans),
        //     }}
        // >
        //     <div className="card__Box">
        //         <img className="card__Image" src={projectImage}></img>                
        //     </div>
        // </Container>
    );
}

export default Card;
