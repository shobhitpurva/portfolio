import React, { useState, useEffect } from 'react';
import "./Header.css";

import { Link } from 'react-scroll';
import Bounce from 'react-reveal/Bounce';



const Header = ({page, link}) => {
    
    const [show, handleShow] = useState(false);
    const changeHeader = () => {
        if(window.scrollY >= 100) {
            handleShow(true);
        }
        else {
            handleShow(false);
        }
    }

   

    useEffect(() => {
        window.addEventListener("scroll", changeHeader);
        //clean up function (it will fire event listner everytime we scroll soo thats y we use clean up function which is removeEventlistner)
        return () => window.removeEventListener("scroll", changeHeader);
    }, []);

    return (
        
        <div className="header">
            <Bounce top>
            <div className={`header__container ${show && 'header__containerChange'}`}>
                <div className="header__left">
                   <a href="/"><img className="header__logo" src="https://i.imgur.com/ATSz1wT.png" /></a>
                </div>
                <div className="header__right">
                    <h2 className="header__item"><Link to="page3" smooth={true} spy={true}>My Work</Link></h2>
                    <h2 className="header__item"><Link to={link} smooth={true} spy={true}>{page}</Link></h2>
                    <h2 className="header__item"><Link to="footer" smooth={true} spy={true}>Contact</Link></h2>
                </div>
            </div>
            </Bounce>
        </div>
       
    )
}

export default Header;
