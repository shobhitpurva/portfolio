import React from 'react';
import "./Page3.css";
import { Link } from 'react-router-dom';


import "slick-carousel/slick/slick.css";
import "slick-carousel/slick/slick-theme.css";
import Fade from 'react-reveal/Fade';


const Page3 = () => {
    return (
        <div className="page3">
            <Fade left>
            <div className="page3__left">
                <img className="page3__portfolioLaptop" src="https://i.imgur.com/2qm7XJ1.png"/>
            </div>
            </Fade>
            <Fade right>
            <div className="page3__right">
                <h2 className="page3__rightHeader">
                    MY WORK
                </h2>
                <h3 className="page3__rightText">
                    Gallery of my different projects having powerful 
                    functionalities. Some of them are created and designed by
                    me and few of them are created with some amazing people 
                    around the globe.
                </h3>
            
                <Link className="page3__rightButton" to ="/Work">
                    Explore More 
                </Link>
            </div>
            </Fade>
        </div>
    )
}

export default Page3;
