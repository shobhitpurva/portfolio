import React from 'react';
import "./Testimonial.css";
const Testimonial = ({
    testImg,
    message,
    nameTitle,
    workTitle
}) => {
    return (
        <div className="testimonial">
            <div className="container">
                <div className="box"> 
                    <div className="imgBox">
                        <img className="testImg" src={testImg} alt=""></img>
                    </div>
                    <p>
                       {message}
                    </p>
                    <h4>
                        {nameTitle}
                        <br />
                        <span>{workTitle}</span>
                    </h4>
                </div>
            </div>
        </div>
    )
}

export default Testimonial;
