import './App.css';
import React from 'react';
import Header from './components/Header.js';
import Footer from './components/Footer.js';
import ScrollToTop from './components/ScrollToTop';
import Banner from './components/Banner.js';
import Page2 from './components/Page2.js';
import Page3 from './components/Page3.js';
import Page4 from './components/Page4.js';
import Page5 from './components/Page5.js';

import Work from "./components/Work";
import { BrowserRouter as Router, Switch, Route } from 'react-router-dom';



function App() {
  return (
    <Router>
      <div className="app">
      <ScrollToTop />
        <Switch>
          <Route path="/Work">
            <Work />
          </Route>
          <Route path="/">
            <Header 
              page="About Me"
              link="page2"
            />
            <Banner />
            <Page3 />
            <Page2 />
            <Page4 />
            <Page5 />
            <Footer />
          </ Route>
          
        </Switch>
      </div>
    </ Router>
  );
}

export default App;
