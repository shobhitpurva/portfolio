import React from 'react'
import "./Banner.css";
import Fade from 'react-reveal/Fade';
function Banner() {
    return (
        <div className="banner">
            <Fade left>
            <div className="banner__showcase">
                <h3>SHOBHIT PURVA</h3>
                <h2>Designer<br /><span className="banner__span">Web Developer</span><br /> & Mentor</h2> 
                <p></p>
                <a className="banner__button" href="https://drive.google.com/drive/folders/1_wJkhNGifWGMwoGNhL5Z3YH_XAlnWMwz?usp=sharing">RESUME</a>
            </div>
            </Fade>
        </div>
    )
}

export default Banner;
