import React from 'react';
import "./Page2.css";
import Fade from 'react-reveal/Fade';
import Zoom from 'react-reveal/Zoom';
const Page2 = () => {   
    return (
        <div className="page2">
            <div className="page2__Box">
                <Fade left>
                <div className="page2__Left">
                    <h2 className="page2__Head">ABOUT ME</h2>
                    <div className="page2__Bottom">
                        <p>
                            I've always been fascinated by art, 
                            long before I stumbled upon designing. 
                            I absolutely love the creative process
                            of designing a new product which can 
                            sometimes be addictive in a good way. 
                            It all started with crafting and 
                            drawing imaginary pieces when I was younger,
                            until the day I picked up my lappy and 
                            discovered another medium to express myself.
                            Presenting artistic ideas and building up
                            optimizing user experience through innovative solutions
                            and dynamic interface designs is
                            one of the most important parts of my work.
                            I feel strongly about making the web application
                            that I can connect with. Even though it is the
                            imagination of my subjects that I'm creating,
                            I find a reflection of myself in them.
                            I'm endlessly inspired by nature it gives 
                            inspiration for new ideas. 
                            I thoroughly enjoy meeting new people and love
                            spend time working on new projects and ideas.
                        </p>
           
                    </div>
                    
                </div>
                </Fade>
                {/* <div className="page2__Right">
                    <img src="https://i.imgur.com/Gn2fVFD.png" className="page2__RightImage" />
                </div>  */}
            </div>
        </div>
        
    )
}

export default Page2;
