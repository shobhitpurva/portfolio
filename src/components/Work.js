import React from 'react';
import './Work.css';
import Card from './Card.js';
import Header from './Header.js';
import Footer from './Footer.js';
import Fade from 'react-reveal/Fade';
function Work() {
    return (
        <div className="work">
                <Header className="work__header"
                        page="Projects"
                        link="card"
                />
                
                <div className="work__banner">
                        <img className="work__bannerImage" src="https://i.imgur.com/BPQ6GUl.png" />
                        <Fade>
                        <div className="work__bannerContent">
                                <h2>My Work</h2>
                                <h3>Showcase of my projects</h3>
                                <a href="https://www.behance.net/shobhitpurva"><button className="work__bannerButton">Case Studies</button></a>
                        </div>
                        </Fade>
                </div>
                
                <div className="work__container">
                <div className="work__cards">
                        <Card 
                                container="card__container"
                                projectName="Enact"
                                projectImage="https://i.imgur.com/otDyOKH.png"
                                Detail1="Enact is a communication app 
                                        that connects people through 
                                        chat messages. Here we can also 
                                        build our own channels to chat in 
                                        groups. 
                                        Enact also consists of google authentication.
                                        Enact follows the latest design pattern of glassmorphism
                                        which increases the user experience.
                                        "
                                languageLogo1="https://i.imgur.com/CSQG5p9.png"
                                languageLogo2="https://i.imgur.com/e5dCgYP.png"
                                languageLogo3="https://i.imgur.com/SJTBhgr.png"
                                languageName1="React"
                                languageName2="Redux"
                                languageName3="Firebase"
                                projectCode="https://bitbucket.org/shobhitpurva/enact/src/master/"
                                projectLink="https://enact-a7e19.web.app/"
                        /> 
                        <hr />
                        <Card 
                                container="card__containerReverse"
                                projectName="Just A1"
                                projectImage="https://i.imgur.com/HwY39g7.jpg"
                                Detail1="Just A1 is having a minimal design pattern.
                                        This business to business application is created 
                                        in which a user can SignUp or Login and can register
                                        their business in a particular category by filling a form,
                                        they can also edit and delete their businesses.
                                        Other users can also contact to the business owner as well."
                                languageLogo1="https://i.imgur.com/CSQG5p9.png"
                                languageLogo3="https://i.imgur.com/tnHzYG4.png"
                                languageLogo2="https://i.imgur.com/SJTBhgr.png"
                                languageName1="React"
                                languageName2="Firebase"
                                projectCode="https://bitbucket.org/Pankajrohila6688/justa1/src/master/"
                                projectLink="https://justa1.web.app"
                        /> 
                        <hr />
                        <Card 
                                container="card__container"
                                projectName="As Technosoft"
                                projectImage="https://i.imgur.com/uafXm89.png"
                                Detail1="Fully fledged business websites created from scratch
                                as an internship project. AS Technosoft Provides service of modern data analytics,
                                visualization tools and technologies to kickstart business or carrier. AS Technosoft 
                                gives seemless user experience with having amazing transitions and effects.
                                The website ensures cross browser compatibility and Mobile responsiveness. 
                                "
                                languageLogo1="https://i.imgur.com/RX64xhQ.pngg"
                                languageLogo2="https://i.imgur.com/a6EGyLE.png"
                                languageLogo3="https://i.imgur.com/7jI9gba.png"
                                languageName1="HTML"
                                languageName2="CSS"
                                languageName3="JavaScript"
                                projectLink="https://www.astechnosoft.net/"
                        /> 
                        <hr />
                        <Card 
                                container="card__containerReverse"
                                projectName="Hols"
                                projectImage="https://i.imgur.com/zBhwQom.png"
                                Detail1 = "This application is completely based on
                                react which has a list of different places
                                It has a calender feature by which we can choose
                                the number of guests and select any date month 
                                or year. After searching, user will be redirected
                                to a different page by react routing."
                                languageLogo1="https://i.imgur.com/CSQG5p9.png"
                                languageLogo3="https://i.imgur.com/tnHzYG4.png"
                                languageLogo2="https://i.imgur.com/tnHzYG4.png"
                                languageName1="React"
                                languageName2=""
                                projectCode="https://bitbucket.org/Pankajrohila6688/hols/src/master/"
                                projectLink="https://mystifying-lovelace-8021c4.netlify.app"
                        />
                                                
                        <hr />
                        <Card 
                                container="card__container"
                                projectName="Efflux"
                                projectImage="https://i.imgur.com/R7FlrVr.png"
                                Detail1="Efflux follows amazing design pattern
                                of Hotstar. Along with having an amazing design it
                                also handles firebase authentication for security.
                                It uses Images and the description from Real-time
                                database with cloud firestore.        
                                "
                                languageLogo1="https://i.imgur.com/CSQG5p9.png"
                                languageLogo2="https://i.imgur.com/e5dCgYP.png"
                                languageLogo3="https://i.imgur.com/SJTBhgr.png"
                                languageName1="React"
                                languageName2="Redux"
                                languageName3="Firebase"
                                projectCode="https://bitbucket.org/shobhitpurva/efflux/src/master/"
                                projectLink="https://efflux-b7c31.web.app/"
                        />
                        <hr />
                        <Card 
                                container="card__containerReverse"
                                projectName="Glue"
                                projectImage="https://i.imgur.com/Oky1e7K.png"
                                Detail1="Glue is a small Todo application where
                                you can add your tasks to manage your schedule activities.
                                Glue design pattern is inspired from latest window 11 UI.
                                The whole backend is handled by cloud firestore."
                                languageLogo1="https://i.imgur.com/RX64xhQ.pngg"
                                languageLogo2="https://i.imgur.com/a6EGyLE.png"
                                languageLogo3="https://i.imgur.com/7jI9gba.png"
                                languageName1="HTML"
                                languageName2="CSS"
                                languageName3="JavaScript"
                                projectCode="https://bitbucket.org/shobhitpurva/glue/src/master/"
                                projectLink="https://glue-wheat.vercel.app/"
                        />
                </div>
                </div>
                <div className="work__footer">
                        <Footer />
                </div>
               
        </div>
    )
}

export default Work
