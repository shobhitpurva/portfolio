import React, { useState } from 'react';
import "./Footer.css";

import MailIcon from '@material-ui/icons/Mail';
import LinkedInIcon from '@material-ui/icons/LinkedIn';



import InstagramIcon from '@material-ui/icons/Instagram';

import Fade from 'react-reveal/Fade';
import emailjs from 'emailjs-com';
import GitHubIcon from '@material-ui/icons/GitHub';
// import Fade from 'react-reveal/Fade';





function Footer() {
    const sendEmail = (e) => {

        e.preventDefault();    //This is important, i'm not sure why, but the email won't send without it
    
        emailjs.sendForm('service_eiox7d2', 'template_2hqftie', e.target, 'user_u6wjqdMA3hu37qjX0G7Tw')
          .then((result) => {
            console.log(result.text); //  window.location.reload()  //This is if you still want the page to reload (since e.preventDefault() cancelled that behavior) 
          }, (error) => {
              console.log(error.text);
          });
        console.log('Email sent');
        setName("");
        setEmail("");
        setMessage("");
    }
    const[name, setName] = useState("");
    const[email, setEmail] = useState("");
    const[message, setMessage] = useState("");

    const [contact, setContact] = useState(false);

    return (
        <Fade bottom>
        <div className="footer">
            <div className="footer__container">
                <div className="footer__top">
                    {/* <div className="footer__left">
                        <img className="footer__logo" src="https://i.imgur.com/PXxTsST.png" />
                        <h2>Quick Links</h2>
                        <h3 className="footer__leftItem">Home</h3>
                        <h3 className="footer__leftItem">My Work</h3>
                        <h3 className="footer__leftItem">About Me</h3>
                        <h3 className="footer__leftItem">My Skills</h3>
                    </div> */}
                    {/* <hr className="footer__line" /> */}

                    {contact ? (

                        <form onSubmit={sendEmail} className="form__div">
                            <h2>Contact</h2>
                            <div className="form__container">
                                {/* <h3 className="form__label">Name</h3> */}
                                <input className="form__input" placeholder="Name" required 
                                    type="text"
                                    name = "name"
                                    value = {name}
                                    onChange = {
                                        (e)=> setName(e.target.value)
                                    } 
                                />
                                {/* <h3 className="form__label">Email</h3> */}
                                <input className="form__input" placeholder="Email" required
                                     type="email"
                                     email = "email"
                                     value = {email}
                                     onChange = {
                                         (e)=> setEmail(e.target.value)
                                     } 
                                />
                                {/* <h3 className="form__label">Message</h3> */}
                                <textarea className="form__textarea" placeholder="Message" required
                                     type="text"
                                     message = "message"
                                     value = {message}
                                     onChange = {
                                         (e)=> setMessage(e.target.value)
                                     } 
                                />
                                <button className="form__btn" type="submit">Send a message</button>
                            </div>
                        </form>
                    ):(
                        <div className="footer__right">
                            <img className="footer__coffeeImage" src="https://i.imgur.com/zDYvkmX.png"/>
                            <h2 className="footer__contactText">
                                Want to create<br /> 
                                something together ?
                            </h2>

                            <button onClick={() => setContact(true)}>Get in touch</button>
                        </div>
                    )}

                    
                </div>
                <div className="footer__bottom">
                    <a href="https://www.instagram.com/shobhitpurva/"><div className="footer__iconContainer">
                        <InstagramIcon className="footer__bottomIcon"/>
                    </div></a>

                    <a href="https://www.linkedin.com/in/shobhit-purva-1085aa164/"><div className="footer__iconContainer">
                        <LinkedInIcon className="footer__bottomIcon" />
                    </div></a>

                    <a href="mailto:purvashobhit@gmail.com?"><div className="footer__iconContainer">
                        <MailIcon className="footer__bottomIcon" />
                    </div></a>   

                    <a href="https://bitbucket.org/shobhitpurva/"><div className="footer__iconContainer">
                        <GitHubIcon className="footer__bottomIcon" />
                    </div></a>   
                </div>
            </div>
        </div>
        </Fade>
    )
}

export default Footer;
