import React from 'react';
import "./Page4.css";
// import FlipCard from "./FlipCard";
import FlipCardSkills from "./FilpCardSkills";


const Page4 = () => {
    return (
        <div className="page4">
            <div className="page4__Top">
                <h2 className="page4__Head">My Skills</h2>
            </div>
           

            <div className="page4__Bottom">
                    
                    <FlipCardSkills
                        headImage="/side__skills.png"
                        heading="Tools"
                        subhead="Explore the tools that I usually prefer"
                        back1="VS Code"
                        back2="Adobe Xd"
                        back3="Figma"
                        back4="Codepen"
                        back5="Material UI"
                        back6="Filmora"
                        back7="Photshop(Basics)"
                        back8="Slack"
                        back9="Github"
                        back10="MailChimp"
                        img1="/logos/vs code.png"
                        img2="/logos/adobe xd.png"
                        img3="/logos/figma.png"
                        img4="/logos/codepen.png"
                        img5="/logos/materialUI.png"
                        img6="/logos/filmora.png"
                        img7="/logos/photoshop.png"
                        img8="/logos/slack.png"
                        img9="/logos/github.png"
                        img10="/logos/mailchimp.png"
                    />
                  

       
                    <FlipCardSkills
                        headImage="/skills.png"
                        heading="Dev Skills"
                        subhead="Explore my Developer Skills"
                        back1="HTML"
                        back2="CSS"
                        back3="JavaScript"
                        back4="React"
                        back5="Node Js (learning)"
                        back6="Firebase"
                        back7="Solidity (learning)"
                        img1="/logos/html.png"
                        img2="/logos/css.png"
                        img3="/logos/javascript.png"
                        img4="/logos/react.png"
                        img5="/logos/nodejs.png"
                        img6="/logos/firebase.png"
                        img7="/logos/solidity.png"
                    />
                   
                    
                    <FlipCardSkills
                        headImage="/tools.png"
                        heading="Side Skills"
                        subhead="Explore my side intrests"
                        back1="Art"
                        back2="Sketching"
                        back3="Video Editing"
                        back4="Teaching"
                        back5="Email Marketing"
                        back6="UI Designing"
                        back7="Logo Designing"
                        back8=""
                        back9=""
                        back10=""
                        img1="/logos/art.png"
                        img2="/logos/sketching.png"
                        img3="/logos/videoediting.png"
                        img4="/logos/teaching.png"
                        img5="/logos/emailmarketing.png"
                        img6="/logos/uiDesign.png"
                        img7="/logos/logoDesign.png"
                        img8=""
                        img9=""
                        img10=""

                    />
        
            </div>
           
        </div>
    )   
}
export default Page4;
